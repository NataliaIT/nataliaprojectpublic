
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ZadDomImiona {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę imion");
        int number = takeNumber();
        System.out.println("Podaj " + number + " imion");
        ArrayList<String> lista = new ArrayList<>();
        lista = stworzListeImion(number);
        Collections.sort(lista);
        System.out.println(lista);
    }

    public static int takeNumber() {
        Scanner scanner = new Scanner(System.in);
        int numberOfNames = scanner.nextInt();
        return numberOfNames;
    }

    public static ArrayList<String> stworzListeImion(int names) {
        ArrayList<String> lista = new ArrayList<>();
        for (int i = 1; i <= names; i++) {
            Scanner scanner = new Scanner(System.in);
            String name = scanner.nextLine();

            lista.add(name);
        }
        return lista;}
}


